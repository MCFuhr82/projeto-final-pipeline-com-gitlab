### Resumo do aprendizado
Jornada de aprendizagem do GitLab-CI.
Neste projeto, aprendi a criar pipelines com integração e entrega contínua, instalar o GitLab-Runner, os principais conceitos sobre Git, além de aprender a como criar um deploy automatizado.

### Conteúdo do projeto   
- Aprender os gatilhos de Pipeline;
- Instalar agentes Shell e Docker;
- Criar pipeline completa;
- Trabalhar com variáveis e secrets;
- Como criar testes de qualidade e segurança;
- dependências entre tarefas;
- Cache de arquivos durante a pipeline;
- Realizar deploys de forma automática, manual ou agendado;
- Envio de notifcação de sucesso ou falha na execução da pipeline.


### fontes:
- Gitlab integrado ao Teams    
https://docs.gitlab.com/ee/user/project/integrations/microsoft_teams.html#configure-microsoft-teams   
https://docs.microsoft.com/pt-br/microsoftteams/platform/webhooks-and-connectors/how-to/connectors-using?tabs=cURL

- docker-compose wordpress:   
https://docs.docker.com/samples/wordpress/

- Gatilho de pipeline com Curl:   
https://docs.gitlab.com/ee/ci/triggers/#use-curl

- Expirar artefatos:   
https://docs.gitlab.com/ee/ci/yaml/index.html#artifactsexpire_in   
https://docs.gitlab.com/ee/ci/jobs/job_control.html

- Runner Docker   
https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#register-a-runner-that-uses-the-docker-executor
